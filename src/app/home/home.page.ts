import { Component, ViewChild } from '@angular/core';
// import { Network } from '@awesome-cordova-plugins/network/ngx';
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
import { Platform } from '@ionic/angular';
import { GoogleMapsComponent   } from '../components/google-maps/google-maps.component';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild(GoogleMapsComponent) mapComponent: GoogleMapsComponent;

  constructor(
    // private network: Network,
    private geolocation: Geolocation,
    private platform: Platform
  ) {
    
  }

  onMapReady(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
      console.log(resp);
      this.mapComponent.addMarker(resp.coords.latitude, resp.coords.longitude);
  
      
    }).catch((error) => {
      console.log('Error getting location', error);
    }); 
  }
  }
}
