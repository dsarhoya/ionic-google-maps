import { Component, OnInit, Input, Output, Renderer2, ElementRef, Inject, ViewEncapsulation, EventEmitter } from '@angular/core';
import {DOCUMENT} from '@angular/common';

import { Network } from '@awesome-cordova-plugins/network/ngx';
import { Geolocation } from '@awesome-cordova-plugins/geolocation/ngx';
import { Platform } from '@ionic/angular';

declare const google: any;

@Component({
  selector: 'google-maps',
  templateUrl: './google-maps.component.html',
  styleUrls: ['./google-maps.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GoogleMapsComponent implements OnInit {

  @Input('apiKey') apiKey: string;
  @Output() mapReady = new EventEmitter();

  public map: any;
  public markers: any[] = [];
  private mapsLoaded: boolean = false;
  private networkHandler = null;

  constructor(
    private renderer: Renderer2, 
    private element: ElementRef, 
    @Inject(DOCUMENT) private _document, 
    private geolocation: Geolocation,
    private platform: Platform,
    private network: Network
  ){
  }

  ngOnInit(){
    // console.log(this.platform);
    
    this.platform.ready().then(() => {
      console.log('platform ready');
      
      console.log('this.network.type', this.network.type);
      
      if (this.network.Connection.NONE !== this.network.type) {
        this.init();
      }
      
      this.network.onConnect().subscribe(() => {
        this.init();
      });

      // this.init().then((res) => {
      //     console.log("Google Maps ready.")
      // }, (err) => {
      //     console.log(err);
      // });

    });
  }
  private init(): Promise<any> {
      return new Promise((resolve, reject) => {
          this.loadSDK().then((res) => {
              this.initMap().then((res) => {
                this.mapReady.emit();
                resolve(true);
              }, (err) => {
                  reject(err);
              });
          }, (err) => {
              reject(err);
          });
      });
  }
  private loadSDK(): Promise<any> {
      console.log("Loading Google Maps SDK");
      return new Promise((resolve, reject) => {
          if(!this.mapsLoaded){
            
            this.injectSDK().then((res) => {
                resolve(true);
            }, (err) => {
                reject(err);
            });
          }
      });
  }
  private injectSDK(): Promise<any> {
    console.log('injectSDK');
    
    return new Promise((resolve, reject) => {
        console.log('injectSDK in');
          window['mapInit'] = () => {
              this.mapsLoaded = true;
              resolve(true);
          }
          let script = this.renderer.createElement('script');
          script.id = 'googleMaps';
          if(this.apiKey){
              script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
              script.src = 'https://maps.googleapis.com/maps/api/js?callback=mapInit';
          }
          this.renderer.appendChild(this._document.body, script);
      });
  }
  private initMap(): Promise<any> {
      return new Promise((resolve, reject) => {
        this.geolocation.getCurrentPosition().then((position) => {
              console.log(position);
              let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              let mapOptions = {
                  center: latLng,
                  zoom: 15
              };
              this.map = new google.maps.Map(this.element.nativeElement, mapOptions);
              resolve(true);
          }, (err) => {
              reject('Could not initialise map');
          });
      });
  }
  public addMarker(lat: number, lng: number): void {
      let latLng = new google.maps.LatLng(lat, lng);
      let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: latLng
      });
      this.markers.push(marker);
  }

}
