import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GoogleMapsComponent } from './google-maps/google-maps.component'
// import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [GoogleMapsComponent],
  imports: [
    CommonModule,
    // CoreModule
  ],
  exports: [GoogleMapsComponent]
})
export class ComponentsModule { }
